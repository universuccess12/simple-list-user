import styled from 'styled-components'

const WrapApp = styled.div`
    text-align: center;

    .total {
        font-size: 24px;
    }

    .wrap-user {
        display: grid;
        grid-template-columns: repeat(4, 1fr);
        place-items: center;
    }
    
`

export default WrapApp