import React, { useState } from 'react'

// apis
import { avatarLink } from '../../apis'

// component
import EditForm from './EditForm';

// styles
import WrapUser from './index.styles'

const User = ({ data, setUsers }) => {
    const { id, name, email, phoneNum, link, isLiked } = data
    const [isShowEditForm, setIsShowEditForm] = useState(false);

    const handleDel = () => {
        setUsers(pre => pre.filter(item => item.id !== id))
    }

    const handleLike = () => {
        const newLike = !isLiked
        setUsers(pre => pre.map(item => item.id === id ? ({...data, isLiked: newLike }) : item ))
    }

    return (
        <WrapUser>
            <div className="wrap-img">
                <img src={avatarLink(id)} alt="avatar" />
            </div>
            <div className="name">{name}</div>
            <div className="email">{email}</div>
            <div className="phone-number">{phoneNum}</div>
            <div className="personal-link">{link}</div>
            <div className="wrap-btn">
                <button onClick={() => setIsShowEditForm(true)}>edit</button>
                <button onClick={handleDel}>del</button>
                <button onClick={handleLike}>{isLiked ? "liked": "like"}</button>
            </div>
            { 
                isShowEditForm &&
                <EditForm 
                    data={data}
                    setUsers={setUsers}
                    setIsShowEditForm={setIsShowEditForm}
                    handleClose={() => setIsShowEditForm(false)}
                />
            }
        </WrapUser>
    )
}

export default User
