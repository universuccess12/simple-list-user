import React, { useReducer } from 'react'

// styles
import WrapEditForm from './index.style'

const EditForm = ({ data, setUsers, setIsShowEditForm, handleClose }) => {
    const { id, name, email, phoneNum, link } = data

    const [editData, setEditData] = useReducer(
        (state, action) => ({ ...state, ...action }),
        {
            name, 
            email, 
            phoneNum, 
            link
        }
    )

    const handleChange = (key) => (e) => {
        setEditData({ [key]: e.target.value })
    } 
    const _handleSave = () => {
        const newUser = {
            ...data,
            ...editData
        }
        setUsers(pre => pre.map(item => item.id === id ? newUser : item))
        setIsShowEditForm(false)
    }

    return (
        <WrapEditForm>
            <div className="wrap-inner-edit-form">
                <div className="field">
                    <label>name: </label>
                    <input type="text" value={editData.name} onChange={handleChange('name')} />
                </div>
                <div className="field">
                    <label>email: </label>
                    <input type="text" value={editData.email} onChange={handleChange('email')} />
                </div>
                <div className="field">
                    <label>phone number: </label>
                    <input type="text" value={editData.phoneNum} onChange={handleChange('phoneNum')} />
                </div>
                <div className="field">
                    <label>link: </label>
                    <input type="text" value={editData.link} onChange={handleChange('link')} />
                </div>
                <div className="wrap-btn">
                    <button onClick={_handleSave}>save</button>
                    <button onClick={handleClose}>cancel</button>
                </div>
            </div>
        </WrapEditForm>
    )
}

export default EditForm
