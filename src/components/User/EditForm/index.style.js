import styled from 'styled-components'

const WrapEditForm = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;

    .wrap-inner-edit-form {
        padding: 20px;
        background-color: #b6b5b5;
        border-radius: 8px;

        .field {
            label {
                display: inline-block;
                width: 100px;
                text-align: left;
                font-size: 15px;
                white-space: nowrap;
            }

            &:not(:first-child) {
                margin-top: 10px;
            }

            input {
                margin-left: 7px;
                outline: none;
                padding: 5px 7px;
                border: none;
                border-radius: 5px;
            }
        }

        .wrap-btn {
            & > *:not(:first-child) {
                margin-left: 10px;
            }
            button {
                margin-top: 20px;
                font-size: 19px;
                border: none;
                outline: none;
                padding: 5px 12px;
                border-radius: 5px;
                cursor: pointer;
            }
        }

    }
`;

export default WrapEditForm