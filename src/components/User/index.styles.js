import styled from 'styled-components'

const WrapUser = styled.div`
    margin: 20px;
    width: 300px;
    background: #80808040;
    padding: 10px;
    box-sizing: border-box;
    border-radius: 20px;

    & > *:not(:first-child) {
        margin-top: 10px;
    }

    .wrap-img {
        width: 100%;
        img {
            width: inherit;
            border-radius: 10px;
        }
    }

    .name, .email, .phone-number, .personal-link {
        font-size: 16px;
    }

    .wrap-btn {
        button {
            &:not(:first-child) {
                margin-left: 5px;
            }
            font-size: 19px;
            border: none;
            outline: none;
            padding: 5px 12px;
            border-radius: 5px;
            cursor: pointer;
        }
    }
`;

export default WrapUser