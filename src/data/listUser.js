const listUser = [
    {
        id: 1,
        name: "Leanne Graham",
        email: "Sincere@april.biz",
        phoneNum: "1-770-736-8031 x56442",
        link: "http://hildegard.org",
        proPercent: 0.2,
        isLiked: false
    },
    {
        id: 2,
        name: "Ervin Howell",
        email: "Shanna@melissa.tv",
        phoneNum: "010-692-6593 x09125",
        link: "http://anastasia.net",
        proPercent: 0.4,
        isLiked: true
    },
    {
        id: 3,
        name: "Clementine Bauch",
        email: "Nathan@yesenia.net",
        phoneNum: "1-463-123-4447",
        link: "http://ramiro.info",
        proPercent: 0.6,
        isLiked: false
    },
    {
        id: 4,
        name: "Patricia Lebsack",
        email: "Julianne.OConner@kory.org",
        phoneNum: "493-170-9623 x156",
        link: "http://kale.biz",
        proPercent: 0,
        isLiked: false
    },
    {
        id: 5,
        name: "Chelsey Dietrich",
        email: "Lucio_Hettinger@annie.ca",
        phoneNum: "(254)954-1289",
        link: "http://demarco.info",
        proPercent: 1,
        isLiked: false
    },
]

export default listUser