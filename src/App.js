import { useState } from 'react'
import styled from 'styled-components'

// data
import listUser from './data/listUser';

// styles
import WrapApp from './App.styles';

// component
import User from './components/User';


function App() {
  const [users, setUsers] = useState(listUser);
  

  return (
    <WrapApp>
      <div className="total">Tổng số user: {users.length}</div>
      <div className="total">Tổng số user đã like: {users.filter(item => item.isLiked).length}</div>
      <div className="wrap-user">
        {users.map(item => <User data={item} key={item.id} setUsers={setUsers} />)}
      </div>
    </WrapApp>
  );
}

export default App;
